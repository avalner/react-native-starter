import React, { FunctionComponent } from 'react';
import { Button as MatButton, ButtonProps as MatButtonProps } from 'react-native-paper';

export interface ButtonProps extends Omit<MatButtonProps, 'children'> {
    label: string;
    zeroMargin?: boolean;
}

const Button: FunctionComponent<ButtonProps> = ({ label, color, style, mode, zeroMargin, onPress, loading, contentStyle, ...other }) => {
    return (
        <MatButton
            style={[{ marginTop: zeroMargin ? 0 : 20 }, style]}
            loading={loading}
            mode={mode || 'contained'}
            contentStyle={{ padding: 4, ...contentStyle as object }}
            color={color}
            onPress={!loading ? onPress : null}
            {...other}
        >
            {label}
        </MatButton>
    )
};


export default Button;
