import React, { FunctionComponent } from "react";
import viewStyles from "../Styles/ViewStyles";
import View, { ViewXProps } from "./View";

export interface SectionProps extends ViewXProps { }

const Section: FunctionComponent<SectionProps> = ({ style, ...other }) => {
	return <View {...other} style={[viewStyles.section, style]} />;
};

export default Section;
