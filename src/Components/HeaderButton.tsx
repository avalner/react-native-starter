/* eslint-disable react-native/no-inline-styles */
import React, { FunctionComponent } from 'react';
import { TouchableX } from '.';
import { View, GestureResponderEvent } from 'react-native';
import { IconX, IconType } from '../Icons';

export interface HeaderButtonProps {
  icon?: string;
  iconOrigin?: IconType;
  onPress?: (event: GestureResponderEvent) => void;
}

const HeaderButton: FunctionComponent<HeaderButtonProps> = ({ onPress, icon, iconOrigin = IconType.ICONICONS }) => {
  return (
    <TouchableX onPress={onPress}>
      <View style={{ padding: 10 }}>
        <IconX name={icon} origin={iconOrigin} color={'white'} size={32} />
      </View>
    </TouchableX>
  );
};

export default HeaderButton;
