import React, { FunctionComponent } from 'react';
import { View, SafeAreaView, StyleProp, ViewStyle } from 'react-native';
import { isIos } from '../Constants';

export interface ViewXProps {
  useSafeAreaView?: boolean;
  style?: StyleProp<ViewStyle>;
}

const ViewX: FunctionComponent<ViewXProps> = ({ children, useSafeAreaView, ...other }) => {
  const Element = useSafeAreaView && isIos ? SafeAreaView : View;
  return <Element {...other}>{children}</Element>;
};

export default ViewX;
