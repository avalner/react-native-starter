import React, { useImperativeHandle, forwardRef, useState, useRef, FunctionComponent, RefForwardingComponent } from 'react';

import { TextInput, Button, TextInputProps } from 'react-native-paper';
import { View } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { IconX } from '../Icons';
import useTheme from '../Themes/Context';
import colors from '../Themes/Colors';

export interface InputRefType {
  focus: () => void;
  blur: () => void;
}

const InputInternal: RefForwardingComponent<InputRefType, TextInputProps> = ({ style, ...other }, ref) => {
  const { theme } = useTheme();

  const inputRef = useRef<TextInput>();

  useImperativeHandle(ref, () => ({
    focus: () => {
      inputRef.current.focus();
    },
    blur: () => {
      inputRef.current.blur();
    },
  }));

  return (
    <TextInput
      ref={inputRef}
      theme={{ colors: { background: colors.white } }}
      {...other}
      style={[{ backgroundColor: colors.transparent }, style]}
    />
  );
}

const Input = forwardRef(InputInternal);

const PasswordInputXInternal: RefForwardingComponent<InputRefType, TextInputProps> = (props, ref) => {
  const thisRef = useRef<InputRefType>();
  useImperativeHandle(ref, () => ({
    focus: () => {
      thisRef.current.focus();
    },
    blur: () => {
      thisRef.current.blur();
    }
  }));

  const [visible, toggleVisibility] = useState(false);

  const toggle = () => {
    toggleVisibility(!visible);
  };

  return (
    <View>
      <Input ref={thisRef} {...props} secureTextEntry={!visible} />
      <View
        style={{
          position: 'absolute',
          justifyContent: 'flex-end',
          top: 0,
          bottom: 0,
          right: 0,
        }}>
        <TouchableOpacity onPress={toggle}>
          <View style={{ padding: 10 }}>
            <IconX name={visible ? 'md-eye' : 'md-eye-off'} />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export const PasswordInputX = React.memo(forwardRef(PasswordInputXInternal));

export default React.memo(Input);
