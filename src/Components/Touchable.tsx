import React, { FunctionComponent, ReactNode, PropsWithChildren } from 'react';
import { InteractionManager, TouchableNativeFeedbackProperties, TouchableOpacityProperties, BackgroundPropType } from 'react-native';
import {
  TouchableNativeFeedback,
  TouchableOpacity,
} from 'react-native-gesture-handler';
import { isAndroid } from '../Constants';
import useTheme from '../Themes/Context';
import { Color } from 'csstype';

export type TouchablePops = PropsWithChildren<{
  foreground?: boolean;
  background?: BackgroundPropType;
  color?: Color;
  border?: boolean;
}> & (TouchableNativeFeedbackProperties | TouchableOpacityProperties);

const Touchable: FunctionComponent<TouchablePops> = ({ onPress, foreground = true, background, color, border, ...other }) => {
  const { theme } = useTheme();

  const _onPress = () => {
    InteractionManager.runAfterInteractions(() => {
      onPress && onPress(null);
    });
  };

  return isAndroid ? (
    <TouchableNativeFeedback
      useForeground={foreground}
      background={TouchableNativeFeedback['Ripple'](
        color || theme.colors.accent,
        border ? false : true,
      )}
      onPress={_onPress}
      {...other}
    />
  ) : (
      <TouchableOpacity onPress={_onPress} {...other} />
    );
};


export default React.memo(Touchable);
