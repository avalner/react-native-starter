import React, { PropsWithChildren, FunctionComponent } from "react";
import { ScrollView, RefreshControl } from "react-native";
import { Container } from ".";

export type LoadingActionContainerProps = PropsWithChildren<{
	onRefresh?: () => void;
	loading?: boolean;
	fixed?: boolean;
}>

const LoadingActionContainer: FunctionComponent<LoadingActionContainerProps> = ({
	onRefresh,
	loading,
	fixed,
	children
}) => {
	return (
		<Container>
			<Header />
			<Content
				onRefresh={onRefresh}
				fixed={fixed}
				loading={loading}
				children={children}
			/>
			<Footer />
		</Container>
	);
};

export default LoadingActionContainer;

const Content = ({ fixed, children, onRefresh, loading }) => {
	let fallback = () => { };

	return fixed ? (
		<Container>{children}</Container>
	) : (
			<ScrollView
				keyboardShouldPersistTaps={"always"}
				nestedScrollEnabled
				refreshControl={
					<RefreshControl
						enabled={onRefresh ? true : false}
						refreshing={onRefresh && loading ? true : false}
						onRefresh={onRefresh || fallback}
					/>
				}
			>
				{children}
			</ScrollView>
		);
};

const Header = () => {
	return null;
};

const Footer = () => {
	return null;
};
