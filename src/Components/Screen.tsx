import { Color } from "csstype";
import React, { FunctionComponent } from "react";
import { StyleProp, ViewStyle } from "react-native";
import viewStyles from "../Styles/ViewStyles";
import useTheme from "../Themes/Context";
import ViewX from "./View";

export interface ContainerProps {
  style?: StyleProp<ViewStyle>;
  bg?: Color;
}

export const Container: FunctionComponent<ContainerProps> = ({ style, bg, children, ...other }) => {
  const { theme } = useTheme();

  return (
    <ViewX
      {...other}
      style={[
        viewStyles.container,
        { backgroundColor: bg ? theme.colors.background : "transparent" },
        style
      ]}
    >{children}</ViewX>
  );
};

export interface ScreenProps {
  style?: StyleProp<ViewStyle>;
}

const Screen: FunctionComponent<ScreenProps> = ({ style, children, ...other }) => {
  return (
    <ViewX
      {...other}
      style={[viewStyles.container, style, { backgroundColor: "#dddddd" }]}
      useSafeAreaView
    >{children}</ViewX>
  );
};

export default Screen;
