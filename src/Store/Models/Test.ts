import { action, thunk, Action, Thunk } from "easy-peasy";

export interface TestModelType {
  istest: string;
  firstName: string;
  lastName: string;
  setTest: Action<TestModelType, string>;
  setName: Action<TestModelType, { key: string; value: any; }>;
  updateName: Thunk<TestModelType>;
}

const actions = {
  // ACTIONS
  setTest: action((state, payload) => {
    state.istest = payload;
  }) as Action<TestModelType, string>,
  setName: action((state, { key, value }) => {
    state[key] = value;
  }) as Action<TestModelType, { key: string; value: any; }>
};

const thunks = {
  // THUNKS
  updateName: thunk((actions, payload) => {
    // Notice that the thunk will receive the actions allowing you to dispatch
    // other actions after you have performed your side effect.
    const first = "HARISH";
    const last = "JANGRA";

    actions.setName({ key: "firstName", value: first });
    actions.setName({ key: "lastName", value: last });
  }) as Thunk<TestModelType>
};

const TestModel: TestModelType = {
  ...actions,
  ...thunks,
  istest: "TEST",
  firstName: "",
  lastName: ""
};

export default TestModel;
