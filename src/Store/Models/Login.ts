import { action, thunk, Thunk, Action } from "easy-peasy";
import { ApiService } from "..";
import {
	setLoginCredentials,
	getLoginCredentials,
	resetLoginCredentials
} from "../../Services/Keychain";
import { Status } from "../../Constants";
import { AppState } from "../../Constants/index";
import BaseModel, { BaseModelType } from "./Base";

import { showErrorToast, showLoading } from "../../Lib/Toast";

export interface LoginPayload {
	username: string;
	password: string;
}

export interface LoginModelType extends BaseModelType {
	appstate: AppState;
	username?: string;
	password?: string;
	changeAppState: Action<LoginModelType, AppState>;
	onLoginInputChange: Action<LoginModelType, { key: string; value: any; }>;
	checkLogin: Thunk<LoginModelType>;
	loginUser: Thunk<LoginModelType, LoginPayload>;
}

const checkLogin: Thunk<LoginModelType> = thunk(async (actions, payload, { dispatch, injections }) => {
	const { api } = injections;

	const credentials = await getLoginCredentials();
	if (credentials) {
		// api.setAuthorizationHeader(credentials.access_token);
		//dispatch.user.requestUserProfile();
		actions.changeAppState(AppState.PRIVATE);
	} else {
		actions.changeAppState(AppState.PUBLIC);
	}
});

const loginUser: Thunk<LoginModelType, LoginPayload> = thunk(async (actions, payload, { dispatch }) => {
	if (!payload.username || !payload.password) {
		return;
	}

	actions.updateStatus(Status.FETCHING);
	// let response = await ApiService.loginUser(payload);

	let response = await setLoginCredentials(
		payload.username,
		payload.password
	);

	//mocking api
	setTimeout(() => {
		actions.updateStatus(response.status ? Status.SUCCESS : Status.FAILED);
		if (!response.status) {
			console.warn(response.error);
		} else {
			actions.changeAppState(AppState.PRIVATE);
		}
	}, 1000);

	// 	ApiService.setAuthorizationHeader(response.data.access_token);
	// 	dispatch.user.requestUserProfile();
});

const LoginModel: LoginModelType = {
	...BaseModel(),
	loginUser,
	checkLogin,
	appstate: AppState.UNKNOWN,
	changeAppState: action((state, payload) => {
		state.appstate = payload;
	}),
	onLoginInputChange: action((state, { key, value }) => {
		console.log('Key:', key, ', Value: ', value);
		state[key.trim()] = value;
	})
};

export default LoginModel;
