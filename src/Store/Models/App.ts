import { action, thunk, Thunk, Action } from "easy-peasy";
import { Status, AppState } from "../../Constants";
import BaseModel, { BaseModelType } from "./Base";

export interface AppModelType extends BaseModelType {
  version?: string;
  setVersion: Action<AppModelType, string>;
  checkAppVersion: Thunk<AppModelType>;
}

const checkAppVersion: Thunk<AppModelType> = thunk(async (actions, payload, { injections }) => {
  const { api } = injections;

  actions.updateStatus(Status.FETCHING);
  // let response = await api.checkAppVersion();
  // if (response.ok) {
  // let version = 9;
  // actions.setVersion(version);
  // }
  actions.updateStatus(Status.SUCCESS);
});

const AppModel: AppModelType = {
  ...BaseModel(),
  checkAppVersion,
  setVersion: action((state, version) => {
    state.version = version;
  })
};

export default AppModel;
