import { action, thunk, Action } from "easy-peasy";
import { Status, AppState } from "../../Constants";

export interface BaseModelType {
  status: Status;
  updateStatus: Action<BaseModelType, Status>;
  mergeState: Action<BaseModelType, any>;
}

const BaseModel = (): BaseModelType => ({
  status: Status.NOT_STARTED,
  updateStatus: action((state, status) => {
    state.status = status;
  }),
  mergeState: action((state, extra) => {
    Object.assign(state, extra);
  })
});

export default BaseModel;
