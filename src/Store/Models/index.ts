import LoginModel, { LoginModelType } from "./Login";
import TestModel, { TestModelType } from "./Test";
import AppModel, { AppModelType } from "./App";

export interface StoreModel {
	test: TestModelType;
	login: LoginModelType;
	app: AppModelType;
}

export default {
	test: TestModel,
	login: LoginModel,
	app: AppModel
} as StoreModel;
