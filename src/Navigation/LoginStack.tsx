import Login from '../Screens/Login';
import { createStackNavigator } from 'react-navigation-stack';
import Routes from './Routes';
import AppIntro from '../Screens/AppIntro';

export default createStackNavigator(
  {
    [Routes.LOGIN_SCREEN]: Login,
    [Routes.APP_INTRO]: AppIntro,
  },
  {
    // Default config for all screens
    // mode:'modal'
    headerMode: 'none',
    initialRouteName: Routes.LOGIN_SCREEN,
  },
);
