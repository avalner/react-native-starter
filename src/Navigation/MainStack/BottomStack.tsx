/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import Routes from '../Routes/index';
import Home from '../../Screens/Home';
import App from '../../Screens/More';
import Profile from '../../Screens/Profile';
import { IconX, IconType } from '../../Icons';

const HomeStack = createStackNavigator({ Home });
const ProfileStack = createStackNavigator({ Profile });
const MoreStack = createStackNavigator({ App });

const BottomTabs = createMaterialBottomTabNavigator(
  {
    [Routes.HOME_SCREEN]: {
      screen: HomeStack,
      path: 'home',
      navigationOptions: {
        title: 'HOME',
        tabBarLabel: 'HOME',
        tabBarIcon: getHomeIcon,
      },
    },
    [Routes.PROFILE_SCREEN]: {
      screen: ProfileStack,
      path: 'students',
      navigationOptions: {
        title: 'PROFILE',
        tabBarLabel: 'PROFILE',
        tabBarIcon: getProfileIcon,
      },
    },
    [Routes.MORE_SCREEN]: {
      screen: MoreStack,
      path: 'more',
      navigationOptions: {
        tabBarIcon: getNotificationIcon,
        tabBarLabel: 'MORE',
        title: 'MORE',
      },
    },
  },
  {
    initialRouteName: Routes.HOME_SCREEN,
    activeColor: 'red',
    shifting: false,
    labeled: true,
    inactiveColor: 'rgba(255,255,255,0.4)',
  },
);

function getHomeIcon({ focused, horizontal, tintColor }) {
  return (
    <IconX
      style={{ marginBottom: 5 }}
      origin={IconType.OCTICONS}
      name={'home'}
      color={tintColor}
    />
  );
}

function getProfileIcon({ focused, horizontal, tintColor }) {
  return (
    <IconX
      style={{ marginBottom: 5 }}
      origin={IconType.FEATHER_ICONS}
      name={'users'}
      color={tintColor}
    />
  );
}

function getNotificationIcon({ focused, horizontal, tintColor }) {
  return (
    <IconX
      style={{ marginBottom: 5 }}
      origin={IconType.ANT_ICON}
      name={'notification'}
      color={tintColor}
    />
  );
}

export default createStackNavigator(
  {
    bottomTabs: BottomTabs,
  },
  {
    headerMode: 'none',
    navigationOptions: {
      header: null,
    },
    initialRouteName: 'bottomTabs',
  },
);
