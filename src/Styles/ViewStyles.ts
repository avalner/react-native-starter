import { StyleSheet } from "react-native";
import metrics from "../Themes/Metrics";

const viewStyles = StyleSheet.create({
  container: {
    flex: 1
  },

  section: {
    padding: metrics.s20,
    paddingHorizontal: metrics.s40
  },

  row: {
    flexDirection: "row",
    alignItems: "center"
  },

  rowSpread: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },

  center: {
    alignItems: "center",
    justifyContent: "center"
  },

  justifyCenter: {
    justifyContent: "center"
  }
});

export default viewStyles;
