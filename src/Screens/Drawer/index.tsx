import React from 'react';
import { ScrollView, Text } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { Section } from '../../Components';
import viewStyles from '../../Styles/ViewStyles';

const Drawer = props => {
  return (
    <ScrollView>
      <SafeAreaView
        style={viewStyles.container}
        forceInset={{ top: 'always', horizontal: 'never' }}>
        <Section style={{ paddingTop: 100, backgroundColor: 'white' }}>
          <Text style={{ fontSize: 20 }}>App Drawer</Text>
        </Section>
      </SafeAreaView>
    </ScrollView>
  );
};

export default Drawer;
