/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { Text } from 'react-native';
import { Container } from '../../Components';
import LoadingActionContainer from '../../Components/LoadingActionContainer';
import NavigationStyles from '../../Styles/NavigationStyles';
import useTheme from '../../Themes/Context';
import { NavigationStackScreenComponent } from 'react-navigation-stack';
import { NavigationParams } from 'react-navigation';
import { ScreenProps } from '../../types';

const ProfileScreen: NavigationStackScreenComponent<NavigationParams, ScreenProps> = ({ navigation }) => {
  const { theme } = useTheme();

  return (
    <LoadingActionContainer fixed>
      <Container
        style={{
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text style={{ fontSize: 20, padding: 20, textAlign: 'center' }}>
          Profile Screen
        </Text>
      </Container>
    </LoadingActionContainer>
  );
};

ProfileScreen.navigationOptions = ({ navigation, screenProps }) => {
  const { theme } = screenProps;
  return {
    headerStyle: [
      NavigationStyles.header_statusBar,
      { backgroundColor: theme.colors.header },
    ],
    headerTitle: 'PROFILE',
    headerTintColor: theme.colors.headerTitle,
    headerTitleStyle: [
      NavigationStyles.headerTitle,
      { color: theme.colors.headerTitle },
    ],
  };
};

export default ProfileScreen;
