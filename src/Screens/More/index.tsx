/* eslint-disable react-native/no-inline-styles */
import React, { useCallback } from 'react';
import { Text } from 'react-native';
import { ButtonX, Container } from '../../Components';
import LoadingActionContainer from '../../Components/LoadingActionContainer';
import { LOCALES } from '../../Constants/index';
import useTranslation from '../../i18n';
import useAuth from '../../Services/Auth';
import NavigationStyles from '../../Styles/NavigationStyles';
import useTheme from '../../Themes/Context';
import { NavigationStackScreenComponent } from 'react-navigation-stack';
import { NavigationParams } from 'react-navigation';
import { ScreenProps } from '../../types';

const MoreScreen: NavigationStackScreenComponent<NavigationParams, ScreenProps> = ({ navigation }) => {
  const { logout } = useAuth();
  const { theme } = useTheme();

  const { t, localeProvider, changeLocale } = useTranslation();

  const _changeLocale = useCallback(() => {
    changeLocale(
      localeProvider.id === LOCALES.HINDI.id ? LOCALES.ENGLISH : LOCALES.HINDI,
    );
  }, [changeLocale, localeProvider.id]);

  return (
    <LoadingActionContainer fixed>
      <Container
        style={{
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text style={{ fontSize: 24, color: theme.colors.primary }}>
          {t('welcome')}
        </Text>

        <ButtonX dark={true} label={t('logout')} onPress={logout} />

        <ButtonX
          dark={true}
          mode="outlined"
          label={t('change_locale')}
          onPress={_changeLocale}
        />
      </Container>
    </LoadingActionContainer>
  );
};

MoreScreen.navigationOptions = ({ navigation, screenProps }) => {
  const { t, theme } = screenProps;
  return {
    headerStyle: [
      NavigationStyles.header_statusBar,
      { backgroundColor: theme.colors.header },
    ],
    headerTitle: 'MORE',
    headerTintColor: theme.colors.headerTitle,
    headerTitleStyle: [
      NavigationStyles.headerTitle,
      { color: theme.colors.headerTitle },
    ],
  };
};

export default MoreScreen;
