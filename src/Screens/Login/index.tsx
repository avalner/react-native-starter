/* eslint-disable react-native/no-inline-styles */
import React, { useRef, useCallback } from 'react';
import { Keyboard, Text, Image, StyleSheet, View } from 'react-native';
import { ButtonX, Container, InputX, PasswordInputX, Section } from '../../Components';
import LoadingActionContainer from '../../Components/LoadingActionContainer';
import BottomPanel, { BottomPanelRefType } from '../../Components/Panel';
import { Status } from '../../Constants';
import useTranslation from '../../i18n';
import { showInfoToast } from '../../Lib/Toast';
import useAuth from '../../Services/Auth';
import { useStoreActions, useStoreState } from '../../Store/hooks';
import useTheme from '../../Themes/Context';
import theme from '../../Themes/configs/black';
import { InputRefType } from '../../Components/Input';
import Logo from '../../Images/28stone_logo_vector.svg';
import colors from '../../Themes/Colors';
import { facebook } from '../../Themes/custom-icons';

const styles = StyleSheet.create({
  logoSection: {
    alignItems: 'center'
  },
  logo: {
    marginTop: 50,
    marginBottom: -10
  },
  headerText: {
    fontSize: 24,
    color: theme.colors.primary,
  }
});

export default () => {
  const onChange = useStoreActions(actions => actions.login.onLoginInputChange);
  const { t } = useTranslation();
  const { login } = useAuth();
  const { theme } = useTheme();

  const inputUserName = useRef<InputRefType>();
  const inputPassword = useRef<InputRefType>();

  const panelRef = useRef<BottomPanelRefType>();

  const onSubmit = () => {
    inputPassword.current.focus();
  };

  const { username, password, status } = useStoreState(state => ({
    username: state.login.username,
    password: state.login.password,
    status: state.login.status,
  }));

  const loginUser = useCallback(() => {
    Keyboard.dismiss();

    if (!username || !password) {
      showInfoToast('Username and password are mandatory, try again !');
    }

    login({
      username,
      password,
    });
  }, [username, password]);

  const continueWithFacebook = useCallback(() => {
    Keyboard.dismiss();
  }, []);

  const loading = status == Status.FETCHING;

  return (
    <Container>
      <LoadingActionContainer>
        <Section style={styles.logoSection}>
          <Logo style={styles.logo} width={300} height={150} />
          <Text style={styles.headerText}>Sign In or</Text>
          <Text style={styles.headerText}>Create an Account</Text>
        </Section>
        <Section>
          <InputX
            label="E-mail Address"
            ref={inputUserName}
            autoCapitalize="none"
            returnKeyType={'next'}
            onSubmitEditing={onSubmit}
            onChangeText={
              text =>
                onChange({
                  key: 'username',
                  value: text,
                })
            }
            value={username}
          />
          <PasswordInputX
            ref={inputPassword}
            value={password}
            label="Password"
            returnKeyType={'go'}
            onSubmitEditing={loginUser}
            onChangeText={
              text =>
                onChange({
                  key: 'password',
                  value: text,
                })
            }
          />
        </Section>
        <Section>
          <ButtonX
            loading={loading}
            dark={true}
            color={loading ? theme.colors.accent : theme.colors.primary}
            onPress={loginUser}
            label="Continue"
          />
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
            <View style={{ flex: 1, height: 1, backgroundColor: colors.black }} />
            <Text style={{ fontSize: 16, fontWeight: 'bold', paddingHorizontal: 25 }}>OR</Text>
            <View style={{ flex: 1, height: 1, backgroundColor: colors.black }} />
          </View>
          <ButtonX
            icon={facebook}
            dark={true}
            color={colors.facebook}
            onPress={continueWithFacebook}
            label="Continue with Facebook"
          />
          <ButtonX
            mode={'text'}
            onPress={() => panelRef.current.show()}
            label=" NEED HELP "
          />
        </Section>
      </LoadingActionContainer>

      <BottomPanel ref={panelRef} />
    </Container>
  );
};
