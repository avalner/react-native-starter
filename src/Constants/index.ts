import { Platform } from "react-native";
import { Dictionary } from "../types";

export const isAndroid = Platform.OS === "android" ? true : false;
export const isIos = !isAndroid;

export enum AppState {
	PUBLIC = "PUBLIC_LOGIN",
	PRIVATE = "MAIN_APP",
	AUTH = "CHECKING_LOGIN",
	UNKNOWN = "UNKNOWN"
};

export enum Status {
	SUCCESS = "SUCCESS",
	NOT_STARTED = "NOT_STARTED",
	FETCHING = "FETCHING",
	FAILED = "FAILED"
};

export interface Locale {
	id: number;
	name: string;
	label: string;
}

export const LOCALES: Dictionary<Locale> = {
	ENGLISH: { id: 1, name: "en", label: "ENGLISH" },
	HINDI: { id: 2, name: "hi", label: "हिंदी" }
};
