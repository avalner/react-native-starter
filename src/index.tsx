/**
 * React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
// import _ from "lodash";
// global._ = _;

import React from "react";
import { StyleSheet, Platform } from "react-native";
import App from "./App";

export default App
