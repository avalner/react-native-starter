import React, { FunctionComponent } from "react";

import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";
import Octicons from "react-native-vector-icons/Octicons";
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { Color, PaddingLeftProperty } from "csstype";

export enum IconType {
	ICONICONS = "ionicons",
	ANT_ICON = "ant",
	EVIL_ICONS = "EVIL",
	FONT_AWESOME = "FONTAWESOME",
	FONT_AWESOME5 = "fontawwesome5",
	MATERIAL_ICONS = "MaterialIcons",
	FEATHER_ICONS = "FEATHER",
	ENTYPO = "ENTYPO",
	OCTICONS = "OCTICONS",
	MATERIAL_COMMUNITY = "MATERIALCOMMUNITY"
};

export interface IconXProps {
	origin?: IconType;
	name?: string;
	color?: Color;
	size?: number;
	paddingLeft?: number;
	style?: any;
}

export const IconX: FunctionComponent<IconXProps> = ({ origin, name, color, size, paddingLeft, style }) => {
	let colorx = color || "#aaaaaa";
	let sizex = size || 24;
	let namex = name || "right";
	let paddingx = paddingLeft || null;

	let Element = Ionicons;

	switch (origin) {
		case IconType.ANT_ICON:
			Element = AntDesign;
			break;

		case IconType.ENTYPO:
			Element = Entypo;
			break;

		case IconType.MATERIAL_ICONS:
			Element = MaterialIcons;
			break;

		case IconType.FONT_AWESOME5:
			Element = FontAwesome5;
			break;

		case IconType.FEATHER_ICONS:
			Element = Feather;
			break;

		case IconType.EVIL_ICONS:
			Element = EvilIcons;
			break;

		case IconType.FONT_AWESOME:
			Element = FontAwesome;
			break;

		case IconType.OCTICONS:
			Element = Octicons;
			break;
		case IconType.MATERIAL_COMMUNITY:
			Element = MaterialCommunityIcons;
			break;

		default:
			Element = Ionicons;
			break;
	}

	return (
		<Element
			name={namex}
			size={sizex}
			color={colorx}
			style={[{ paddingLeft: paddingx }, style]}
		/>
	);
};
