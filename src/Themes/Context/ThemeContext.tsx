import React, { useRef, useState, PropsWithChildren, FunctionComponent } from "react";
import defaultTheme from "..";
import { Theme } from "react-native-paper";
import { CustomTheme } from "../configs/default";

export interface ThemeContextValue {
	theme: CustomTheme;
	changeTheme: (theme: Theme) => void;
}

const ThemeContext = React.createContext<ThemeContextValue>(null);

export type ThemeProviderProps = PropsWithChildren<{
	theme?: CustomTheme;
}>;

export const ThemeProvider: FunctionComponent<ThemeProviderProps> = ({ theme, children }) => {
	const [themeObj, changeTheme] = useState<CustomTheme>(theme || defaultTheme)

	const setTheme = (theme: CustomTheme) => {
		changeTheme(theme)
	}

	return (
		<ThemeContext.Provider
			value={{
				theme: themeObj,
				changeTheme: (theme: CustomTheme) => setTheme(theme)
			}}
		>
			{children}
		</ThemeContext.Provider>
	);
}

export default ThemeContext;
