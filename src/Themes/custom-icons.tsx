import React from 'react';
import AwesomeIcon from 'react-native-vector-icons/FontAwesome';

export const facebook = ({ size, color }) => (
  <AwesomeIcon name="facebook-square" size={size} color={color} />
);