import { DefaultTheme } from "react-native-paper";
import colors from "../Colors";
import { CustomTheme } from "./default";

const theme: CustomTheme = {
  ...DefaultTheme,
  dark: false,
  roundness: 7,
  colors: {
    ...DefaultTheme.colors,
    primary: colors.black,
    accent: colors.musturd,
    background: colors.issabeline,
    text: colors.panegrey,
    placeholder: colors.ashgrey,
    header: colors.black,
    headerTitle: colors.white,

    //react-native-paper theme colors
    surface: colors.white,
    primaryText: colors.darkgunmetal
  }
};

export default theme;
