import { DefaultTheme, Theme } from 'react-native-paper';
import colors from '../Colors';

export type CustomTheme = Theme & {
  colors: {
    header: string;
    headerTitle: string;
    primaryText: string;
  }
};

const theme: CustomTheme = {
  ...DefaultTheme,
  dark: true,
  roundness: 7,
  colors: {
    ...DefaultTheme.colors,
    primary: colors.tiffanyBlue,
    accent: colors.flame,
    background: colors.issabeline,
    text: colors.panegrey,
    placeholder: colors.ashgrey,
    header: colors.black,
    headerTitle: colors.white,

    //react-native-paper theme colors
    surface: colors.white,
    primaryText: colors.darkgunmetal,
  },
};

export default theme;
