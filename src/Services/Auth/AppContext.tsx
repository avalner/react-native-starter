import React, { useEffect, useCallback } from 'react';
import { Alert } from 'react-native';
import { AppState } from '../../Constants';
import { resetLoginCredentials } from '../Keychain';
import NavigationService, { Routes } from '../../Navigation';
import useCheckVersion from '../CheckVersion';
import { useStoreActions, useStoreState } from '../../Store/hooks';

export interface AppStateContextValue {
  state: AppState;
  logout: () => void;
  login: (data: { username: string, password: string }) => void;
}

const AppStateContext = React.createContext<AppStateContextValue>(null);

export const AppContextProvider = props => {
  const { loginUser, setState, checkLogin } = useStoreActions(actions => ({
    loginUser: actions.login.loginUser,
    setState: actions.login.changeAppState,
    checkLogin: actions.login.checkLogin,
  }));
  useCheckVersion();
  const state = useStoreState(store => store.login.appstate);

  const _logoutUser = useCallback(async () => {
    const reset = resetLoginCredentials();
    if (reset) {
      // do logout
      setState(AppState.PUBLIC);
    }
  }, [setState]);

  const logout = useCallback(() => {
    Alert.alert(
      'Please comfirm Logout',
      'Are you sure you want to logout from the app',
      [
        {
          text: 'Yes, Logout',
          onPress: _logoutUser,
        },
        {
          style: 'cancel',
          text: 'No, Stay here',
        },
      ],
    );
  }, [_logoutUser]);

  const login = useCallback(
    reqData => {
      loginUser(reqData);
    },
    [loginUser],
  );

  // check loggedin on mount
  useEffect(() => {
    state == AppState.UNKNOWN && checkLogin();
  }, [checkLogin, state]);

  // app state reactor
  useEffect(() => {
    if (state == AppState.PRIVATE) {
      NavigationService.navigate(Routes.MAIN_APP);
    } else if (state == AppState.PUBLIC) {
      NavigationService.navigate(Routes.LOGIN_STACK);
    } else {
      //do something if needed
    }
  }, [state]);

  return (
    <AppStateContext.Provider
      value={{
        state,
        logout,
        login,
      }}>
      {props.children}
    </AppStateContext.Provider>
  );
};

export default AppStateContext;
