import I18n from "i18n-js";
import React from "react";
import { LOCALES, Locale } from "../Constants";
import useStorage from "../Services/AsyncStorage";
import "./i18n";
import translateOrFallback from "./TranslateFallback";

export type LocaleContextValue = {
	localeProvider: Locale;
	t: (initialMsg: string, options?: any) => string;
	changeLocale: (locale: Locale) => void;
};

const LocaleContext = React.createContext<LocaleContextValue>(null);

export const LocaleContextProvider = props => {
	const [locale, changeLocale] = useStorage("@language", LOCALES.ENGLISH);
	I18n.locale = locale.name;

	const _changeLocale = locale => {
		I18n.locale = locale.name;
		changeLocale(locale);
	};

	return (
		<LocaleContext.Provider
			value={{
				...I18n,
				localeProvider: locale,
				t: translateOrFallback,
				changeLocale: _changeLocale
			}}
		>
			{props.children}
		</LocaleContext.Provider>
	);
};

export default LocaleContext;
