import { CustomTheme } from "./Themes/configs/default";

export type Dictionary<T> = { [key: string]: T };

export type ScreenProps = {
  theme: CustomTheme;
  t: (initialMsg: string, options?: any) => string;
};