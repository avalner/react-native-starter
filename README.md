# **_react-native-starter_**

### Starter and learning tool for beginners to intermediate. (IOS and Android ready)

## WHY USE EASY STARTER ?

- Always up-to-date React Native [0.61.2](https://github.com/react-native-community/releases/blob/master/CHANGELOG.md#0612) scaffolding.

* [Easy-Peasy](https://github.com/ctrlplusb/easy-peasy) makes it easy to access the global state.

- Use of [React Hooks](https://reactjs.org/docs/hooks-intro.html) optimizes the performance and increases reusability.

* Provides a basic infrastructure, which can easily be used throughout the whole project.

- [ApiSauce](https://github.com/infinitered/apisauce) Axios + standardized errors + request/response transforms.

- TypeScript

## Currently Includes
- TypeScript 
- React-native 0.61
- react-native-keychain
- easy-peasy for state management
- Hooks
- react-navigation
- react-native-vector-icons
- Login Navigation Flow using React Context.
- Themes support and much more
- Deeply integrated, production ready workflow

## Ready to use templates

- Loading /Login / Main app screens
- extended Material Bottom Tabs Navigator (customizable) and Drawer Navigator using react-navigation
- App Intro screens
- Theme context
- Api ready service
- oAuth ready interceptor
- material design components using [react-native-paper](https://github.com/callstack/react-native-paper)
- react-native-modal
- easy-peasy models
- `useStorage()` for async storage
- `useNetInfo()` for network info
- `useTranslation()` for i18n
- `useTheme()` for using themes
- ... much more

## Getting Started

**Step 1:** Clone and Install

```sh

$ git clone git@bitbucket.org:avalner/react-native-starter.git

$ cd react-native-starter

$ npm install

```

**Step 2:** Rename the app [react-native-rename](https://github.com/junedomingo/react-native-rename#installation)

```sh

$ npm install -g react-native-rename

$ react-native-rename <newName>

```

[View](https://github.com/junedomingo/react-native-rename#installation) more option

**Step 3:** Start the App

```sh

$ npm start --reset-cache

$ npm run android:dev

```

### For release build (ANDROID)

see [Generating signed apk](https://facebook.github.io/react-native/docs/signed-apk-android)

```sh

$ npm run android:build

```
